#! /bin/bash
# Author -- meevs
# Creation Date -- 2023-11-03
# File Name -- init_container.sh
# Notes --

# Some useful directory info
echo -e "<<INFO>> -- Server is running out of \"\e[38;2;200;200;0m${INSTALL_DIR}\e[0m\""
echo -e "<<INFO>> -- Plugin directory is \"\e[38;2;200;200;0m${INSTALL_DIR}/plugins\e[0m\""

# Author -- meevs
# Creation Date -- 2024-07-17
# Function Name -- determineLatestPaperBuild
# Function Purpose -- Determines the latest Paper build version for a given Minecraft version
# Function Parameters --
# 	-- MC_VERSION -- The version of Minecraft that will be checked.
# Function Returns -- The latest Paper build version available for the given version of Minecraft
# Notes --
function determineLatestPaperBuild () {
	local LATEST_PAPER_VERSION="?"
	local STATUS=1

	if [[ "${#}" -ge 1 ]]
	then
		local MC_VERSION="${1}"
		local RESPONSE=$(curl -X 'GET' "https://api.papermc.io/v2/projects/paper/versions/${MC_VERSION}" -H 'accept: application/json')	
		local BUILD_COUNT=$(echo "${RESPONSE}" | jq .builds | jq length)
	
		if [[ ! -z "${BUILD_COUNT}" ]]
		then # We have a valid build count
			LATEST_PAPER_VERSION=$(echo "${RESPONSE}" | jq .builds[$((BUILD_COUNT - 1))])
			STATUS=0
		fi
	fi

	echo "${LATEST_PAPER_VERSION}"
	return "${STATUS}"
}

# Author -- meevs
# Creation Date -- 2024-07-17
# Function Name -- dlPaperBuild
# Function Purpose -- Downloads the given Paper build for the given version of Minecraft
# Function Parameters --
# 	-- MC_VERSION -- The version of Minecraft to be used for downloading
# 	-- PAPER_BUILD -- The version of Paper to be used for downloading
# Function Returns -- void
# Notes --
function dlPaperBuild () {
	local STATUS=1

	if [[ "${#}" -ge 2 ]]
	then
		local MC_VERSION="${1}"
		local PAPER_BUILD_VERSION="${2}"

		echo -e "<<INFO>> -- Downloading \e[38;2;200;200;0mv${MC_VERSION}\e[0m build \e[38;2;200;200;0m${PAPER_BUILD_VERSION}...\e[0m"
		curl --location "https://api.papermc.io/v2/projects/paper/versions/${MC_VERSION}/builds/${PAPER_BUILD_VERSION}/downloads/paper-${MC_VERSION}-${PAPER_BUILD_VERSION}.jar" --output "${BUILD_DIR}/paper_mc-${MC_VERSION}-${PAPER_BUILD_VERSION}.jar"
		if [[ -f "${BUILD_DIR}/paper_mc-${MC_VERSION}-${PAPER_BUILD_VERSION}.jar" ]]
		then
			echo -e "\e[38;2;0;200;0m<<INFO>> -- Download of v${MC_VERSION} build ${PAPER_BUILD_VERSION} successful!\e[0m"
			STATUS=0;
		else
			echo -e "\e[38;2;255;0;0m<<ERROR>> -- Failed to download v${MC_VERSION} build ${PAPER_BUILD_VERSION}! Exiting!\e[0m"
		fi
	fi

	return "${STATUS}"
}

# Author -- meevs
# Creation Date -- 2024-07-22
# Function Name -- linkServer
# Function Purpose -- Creates a symbolic link for the desired server jar
# Function Parameters --
# 	-- MC_VERSION -- The version of Minecraft to be used
# 	-- PAPER_BUILD_VERSION -- The build version of Paper to be used
# Function Returns -- void
# Notes --
function linkServer () {
	local STATUS=1

	if [[ "${#}" -ge 2 ]]
	then
		local MC_VERSION="${1}"
		local PAPER_BUILD_VERSION="${2}"

		if [[ -s "${INSTALL_DIR}/paper_mc_server.jar" ]] then
				unlink "${INSTALL_DIR}/paper_mc_server.jar"	
		fi
		
			ln -s "${BUILD_DIR}/paper_mc-${MC_VERSION}-${PAPER_BUILD_VERSION}.jar" "${INSTALL_DIR}/paper_mc_server.jar"
			STATUS="${?}"	
	fi	

	return "${STATUS}"
}

# Check if initial setup has been done
if [[ ! -f "${INSTALL_DIR}/start_server.sh" ]]
then
	cp -v /tmp/paper_mc/start_server.sh ./
fi

# Ensure EULA exists
echo -e "<<INFO>> -- Setting to EULA to true\e[0m"
echo "eula=true" > ./eula.txt

# If no paper build version has been specified
if [[ -z "${PAPER_BUILD_VERSION}" ]]
then
	echo -e "<<INFO>> -- Fetching latest Paper build version..."
	PAPER_BUILD_VERSION="$(determineLatestPaperBuild "${MC_VERSION}")"
	PAPER_BUILD_VERSION_STATUS="${?}"
	if [[ "${PAPER_BUILD_VERSION_STATUS}" -eq 0 ]] then
		echo -e "<<INFO>> -- Found version \e[38;2;0;255;0m${PAPER_BUILD_VERSION}\e[0m!"

		# Check if this version is already downloaded
		if [[ -f "${BUILD_DIR}/paper_mc-${MC_VERSION}-${PAPER_BUILD_VERSION}.jar" ]]
		then
			echo -e "<<INFO>> -- \e[38;2;0;200;0mLatest version is already downloaded!\e[0m"
		else
			dlPaperBuild "${MC_VERSION}" "${PAPER_BUILD_VERSION}"
			linkServer "${MC_VERSION}" "${PAPER_BUILD_VERSION}"
		fi
	else
		echo -e "\e[38;2;255;255;0m<<WARN>> -- Failed to fetch Paper build version! Will attempt to use local version.\e[0m"
	fi
# Check if desired version is not installed
elif [[ ! -f "${BUILD_DIR}/paper_mc-${MC_VERSION}-${PAPER_BUILD_VERSION}.jar" ]]
then
	dlPaperBuild "${MC_VERSION}" "${PAPER_BUILD_VERSION}"
	linkServer "${MC_VERSION}" "${PAPER_BUILD_VERSION}"
fi

if [[ -s "${INSTALL_DIR}/paper_mc_server.jar" ]] then
	# Start server
	./start_server.sh
else
	echo -e "\e[38;2;255;0;0m<<ERROR>> -- No viable jar file located in image! Exiting!\e[0m"
	exit 1
fi

