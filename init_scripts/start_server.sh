#! /bin/sh
# Author -- meevs
# Creation Date -- 2023-03-11
# File Name -- start_server.sh
# Notes --

echo -e "<<INFO>> -- \e[38;2;0;200;0mStarting server...\e[0m"
java -Xms"${MIN_RAM}" -Xmx"${MAX_RAM}" -jar "./paper_mc_server.jar" nogui
echo -e "<<INFO>> -- \e[38;2;0;200;0mServer has stopped...\e[0m"

